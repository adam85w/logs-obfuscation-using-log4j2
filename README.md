# The logs obfuscation using log4j2
The proof of concept of logs obfuscation using the log4j2 library with the spring boot framework.
The log4j adapter was prepared as a implementation of the LogEventPatternConverter with the corresponding configuration file. The most important things in the configuration are package to the adapter class and the omsg key instead of the msg key in a pattern attribute. It is important to use the omsg key because the msg key still contains an original message without obfuscation. There is no possibility to override the msg key.

### Useful links
 * https://logging.apache.org/log4j/2.x/manual/extending.html documentation about extending log4j
 * https://objectpartners.com/2017/09/26/masking-sensitive-data-in-log4j-2/ tutorial
 * https://andrew-flower.com/blog/Create_Custom_Log4j_Plugins tutorial
 
### Tags
 * obfuscation using log4j2
 * masking sensitive information
 * phone numbers obfuscation
 * credit cards obfuscation 
 
### Contact
Adam Woźniak, adam85.w at gmail dot com
