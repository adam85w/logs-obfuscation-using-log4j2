package net.adam85w.logging.obfuscation;

/**
 * The generic class for all obfuscators.
 * 
 * @author adam85w
 */
public abstract class Obfuscator implements Comparable<Obfuscator> {

	/**
	 * The process of obfuscation specified text as a parameter.
	 * 
	 * @param text
	 * @return obfuscated text
	 */
	public abstract String obfuscate(String text);

	/**
	 * Returns the name of an obfuscator, the name should be unique.
	 * 
	 * @return name of an obfuscator
	 */
	public abstract String getName();

	/**
	 * The order of an obfuscator which determines the ordering of execution. The
	 * obfuscator which is higher ordering is executed as first.
	 * 
	 * @return order of an obfuscator
	 */
	public abstract short order();

	@Override
	public int compareTo(Obfuscator o) {
		if (order() > o.order()) {
			return -1;
		} else if (order() < o.order()) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obfuscator other = (Obfuscator) obj;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Obfuscator [getName()=" + getName() + "]";
	}

}
