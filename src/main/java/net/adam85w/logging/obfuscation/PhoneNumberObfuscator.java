package net.adam85w.logging.obfuscation;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class PhoneNumberObfuscator extends Obfuscator {

	private final String name = PhoneNumberObfuscator.class.getName();

	private final Pattern pattern = Pattern.compile("([0-9]{3})([0-9]{3})([0-9]{3})");

	private final String replacement = "$1***$3";

	private final short length = 9;

	@Override
	public String obfuscate(String text) {
		if (!StringUtils.isEmpty(text) && text.length() >= length) {
			return pattern.matcher(text).replaceAll(replacement);
		}
		return text;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public short order() {
		return length;
	}

}
