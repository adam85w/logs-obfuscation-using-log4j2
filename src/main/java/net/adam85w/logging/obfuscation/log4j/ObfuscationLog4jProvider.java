package net.adam85w.logging.obfuscation.log4j;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import net.adam85w.logging.obfuscation.Obfuscator;

public final class ObfuscationLog4jProvider {

	private static SortedSet<Obfuscator> obfuscators = Collections.synchronizedSortedSet(new TreeSet<>());

	private ObfuscationLog4jProvider() {
	}

	public static SortedSet<Obfuscator> getAll() {
		return obfuscators;
	}

	public static void add(Obfuscator obfuscator) {
		obfuscators.add(obfuscator);
	}

	public static void remove(Obfuscator obfuscator) {
		obfuscators.remove(obfuscator);
	}

	public static boolean isEmpty() {
		return obfuscators.isEmpty();
	}

}
