package net.adam85w.logging.obfuscation.log4j;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;

import net.adam85w.logging.obfuscation.Obfuscator;

/**
 * The Obfuscation Log4j Adapter.
 * It is a implementation of the LogEventPatternConverter according to the log4j documentation:
 * https://logging.apache.org/log4j/2.x/manual/extending.html
 * To force log4j to use this adapter is important to put the package name which contains it
 * and use omsg instead of msg in the pattern attribute.
 * 
 * @author adam85w
 */
@Plugin(name = "ObfuscationLog4jAdapter", category = "Converter")
@ConverterKeys({ ObfuscationLog4jAdapter.NAME })
public class ObfuscationLog4jAdapter extends LogEventPatternConverter {

	public static final String NAME = "omsg";

	protected ObfuscationLog4jAdapter(String name, String style) {
		super(name, style);
	}

	public static ObfuscationLog4jAdapter newInstance(String[] options) {
		return new ObfuscationLog4jAdapter(NAME, NAME);
	}

	@Override
	public void format(LogEvent event, StringBuilder toAppendTo) {
		String message = event.getMessage().getFormattedMessage();
		if (!ObfuscationLog4jProvider.isEmpty()) {
			for (Obfuscator obfuscator : ObfuscationLog4jProvider.getAll()) {
				message = obfuscator.obfuscate(message);
			}
		}
		toAppendTo.append(message);
	}
}
