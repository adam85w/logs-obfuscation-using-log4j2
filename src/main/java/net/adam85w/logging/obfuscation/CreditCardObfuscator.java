package net.adam85w.logging.obfuscation;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class CreditCardObfuscator extends Obfuscator {

	private final String name = CreditCardObfuscator.class.getName();

	private final Pattern pattern = Pattern.compile("([0-9]{4})([0-9]{8})([0-9]{4})");

	private final String replacement = "$1********$3";

	private final short length = 16;

	@Override
	public String obfuscate(String text) {
		if (!StringUtils.isEmpty(text) && text.length() >= length) {
			return pattern.matcher(text).replaceAll(replacement);
		}
		return text;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public short order() {
		return length;
	}

}
