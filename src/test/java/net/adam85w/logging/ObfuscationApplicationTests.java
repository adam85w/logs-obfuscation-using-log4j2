package net.adam85w.logging;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import net.adam85w.logging.obfuscation.CreditCardObfuscator;
import net.adam85w.logging.obfuscation.PhoneNumberObfuscator;
import net.adam85w.logging.obfuscation.log4j.ObfuscationLog4jProvider;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ObfuscationApplication.class })
public class ObfuscationApplicationTests {

	public final Logger logger = LoggerFactory.getLogger(ObfuscationApplicationTests.class); 
	
	@Before
	public void setupProvider() {
		ObfuscationLog4jProvider.add(new CreditCardObfuscator());
		ObfuscationLog4jProvider.add(new PhoneNumberObfuscator());
	}
	
	@Test
	public void obfuscatePhoneNumbers() {
		logger.debug("it is a phone number: 908678901");
		logger.debug("it is not a phone number: 90867890");
		logger.debug("a few phone numbers in a row: 908678901 808678902 708678903");
	}
	
	@Test
	public void obfuscateCreditCards() {
		logger.debug("it is a credit card: 1234567812347890");
		logger.debug("it is not a credit card: 123456781234789");
		logger.debug("a few phone numbers in a row: 1234567812347890 2234567812347891 3234567812347892");
	}
	
}
